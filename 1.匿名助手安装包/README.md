软件安装包较大，附上百度云下载连接：
> - 部分电脑可能提示缺少VC运行DLL，请安装https://pan.baidu.com/s/1Mp1PfhdQQEaRpvxKr2v5jA?pwd=ano6

##### 版本：V1.0.13
> - 更新时间：20231108
> - Win版链接：https://pan.baidu.com/s/13aTp79Cxs5fG0btCD07gFQ?pwd=ano6 提取码: ano6 
> - 使用方法：
> - - 本版本需要安装包重新安装
> - 更新内容：
> - - 程序性能优化
> - - 波形数据存储、读取增加时间戳功能
> - - 校准参数写入后，自动清空参数列表，自动重新读取
> - - 增加崩溃检测功能，奔溃后下次打开软件清空设置
> - - 增加界面方案手动保存、读取功能，可保存不同布局方案
> - - 字符串数据帧支持换行符，换行符“\n”
> - - 修复命令参数只能8字节的问题
> - - 协议手册更新
> - - 0x03、0x04、0x05、0x08数据帧内容调整

##### 版本：V1.0.12
> - 更新时间：20231029
> - Win版链接：https://pan.baidu.com/s/1_cPqN1ZPsqxAkuoAPqpvrg?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版本需要安装包重新安装
> - 更新内容：
> - - 程序性能优化
> - - 协议帧显示功能重做，采用表格形式，更加灵活且美观，提升性能
> - - 增加FFT数据分析功能，在数据列表右键需要分析的数据即可进行实时FFT分析
> - - 常用数显支持鼠标拖动排序
> - - 将常用工具界面小工具的按钮调整顺序改为鼠标拖动排序
> - - 添加端口+设备搜索功能，可自动搜索所有串口或本机IP网段，自动连接串口或UDP模式下的设备
> - - 添加多设备支持，添加手动添加设备功能

##### 版本：V1.0.11
> - 更新时间：20231014
> - Win版链接：https://pan.baidu.com/s/105fqeu9dVzNxwscESP3axw?pwd=ano6
> - 使用方法：
> - - 本版为完整安装包，直接安装即可。
> - 更新内容：
> - - 增加波形添加竖向标尺的功能，可记录并显示标尺时间
> - - 连接设置界面自动隐藏非当前选中的连接设置内容，选择串口时只显示串口设置，选择UDP时只显示UDP设置
> - - 3D轨迹功能开放公测
> - - 数据列表数据增加默认缩放
> - - 修改波形刷新逻辑，不再限制50hz（非opengl加速时，opengl加速情况下由显卡决定速度），电脑配置比较好时波形可达100hz刷新率。

##### 版本：V1.0.10
> - 更新时间：20230924
> - Win版链接：https://pan.baidu.com/s/1wqnDm487-8dW1q3j37YNTw?pwd=ano6
> - 使用方法：
> - - 本版为完整安装包，直接安装即可。
> - 更新内容：
> - - 修复波形智能添加数据的BUG，开启智能添加功能后，自动选择频率最快的帧作为波形更新触发。
> - - 新增连接快捷打开关闭按钮，可以不用打开连接配置界面直接快速打开或关闭连接。
> - - 协议模拟器端口修改，对应UDP本地端口12300，模拟器端口12302。
> - - 波形增加全显功能，一屏显示所有数据
> - - 波形鼠标滚轮缩放功能，修改为以鼠标位置为中心点缩放
> - - 波形增加时间戳功能，可记录数据接收到的时间并显示（注意，这里是上位机接收到数据的时间，会由于通信等原因产生延迟）。
> - - 连接断开后，自动清空已连接的设备信息
> - - 优化基本收发界面console模式接收显示，支持提醒色，支持删除字符
> - - 命令界面，将触发按钮调整至最左侧，与命令的对应关系更明显
> - - 修改加速度校准、罗盘校准命令为最新协议格式
> - - 实现基于硬件地址ID的多设备连接功能

##### 版本：V1.0.9
> - 更新时间：20230523
> - Win版链接：阿里云盘：https://www.aliyundrive.com/s/R2u6FuXharQ
> - 使用方法：
> - - 本版为完整安装包，直接安装即可。
> - 更新内容：
> - - 新增波形批量显示、隐藏快捷按钮，可一键显示、隐藏所有波形。
> - - 修复参数列表界面文字错误。
> - - 数据列表界面优化。
> - - 增加关闭界面时窗口状态自动保存开关，使界面应用更加灵活。

##### 版本：V1.0.8
> - 更新时间：20230506
> - Win版链接：https://pan.baidu.com/s/1RMDgUG6YRSeHu9ED85Ib0Q?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版为完整安装包，直接安装即可。
> - 更新内容：
> - - 新增界面保存功能，关闭软件时，会自动保存当前界面布局，下次打开时自动恢复。
> - - 新增界面布局收藏功能，界面布局调整好后，点击软件界面右上角的保存界面按钮，即可保存，同时也可通过恢复按钮恢复保存的界面布局。
> - - 新增显示串口名称的功能。
> - - 修复灵活数据不能保存的bug。

##### 版本：V1.0.7
> - 更新时间：20230418
> - Win版链接：https://pan.baidu.com/s/1GN1GsXYIYd6SCXl1oMmlJw?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版本只更新了主程序exe文件，下载链接只有主程序exe文件，将本版本的exe文件替换到1.0.3版本的安装目录中即可。
> - - 如果没有安装过1.0.3版本，则先安装1.0.3版本，然后替换本版本主程序exe即可。
> - 更新内容：
> - - 参数范围最大值、最小值改为两列单独显示。
> - - 修复未打开常用数显界面是添加数据导致程序崩溃的BUG。
> - - 修复罗盘校准数据错误问题。

##### 版本：V1.0.6
> - 更新时间：20230416
> - Win版链接：https://pan.baidu.com/s/1onhz394zhAoVKm79OKjb_Q?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版本只更新了主程序exe文件，下载链接只有主程序exe文件，将本版本的exe文件替换到1.0.3版本的安装目录中即可。
> - - 如果没有安装过1.0.3版本，则先安装1.0.3版本，然后替换本版本主程序exe即可。
> - 更新内容：
> - - 命令信息文本显示BUG修复。
> - - 增加常用数据放大显示功能。（在数据列表界面，右键点击需要放大显示的数据，选择“添加常用数显”，即可添加数据）
> - - 修复加速度校准、罗盘校准数据通信缩放问题。
> - - 传感器校准3D提示界面默认隐藏，修改为可关闭窗口，增加打开窗口按钮。

##### 版本：V1.0.5
> - 更新时间：20230412
> - Win版链接：https://pan.baidu.com/s/1yKxroq76UNSstBgFfyqOXA?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版本只更新了主程序exe文件，下载链接只有主程序exe文件，将本版本的exe文件替换到1.0.3版本的安装目录中即可。
> - - 如果没有安装过1.0.3版本，则先安装1.0.3版本，然后替换本版本主程序exe即可。
> - 更新内容：
> - - 地图设备坐标可根据0x30帧GPS信息经纬度进行移动显示。
> - - 协议模拟器增加0x30帧模拟功能，设置经纬度后，模拟发送以此经纬度为圆心的坐标值。

##### 版本：V1.0.4
> - 更新时间：20230411
> - Win版链接：https://pan.baidu.com/s/1JCyjzBxoDAzRbY4fcyxRGA?pwd=ano6 提取码：ano6 
> - 使用方法：
> - - 本版本只更新了主程序exe文件，下载链接只有主程序exe文件，将本版本的exe文件替换到1.0.3版本的安装目录中即可。
> - - 如果没有安装过1.0.3版本，则先安装1.0.3版本，然后替换本版本主程序exe即可。
> - 更新内容：
> - - 修复GPS坐标显示科学计数法精度问题，改为整数型显示。
> - - 修改波形缩放比例范围，支持9位小数。

##### 版本：V1.0.3
> - 更新时间：20230406
> - Win版链接：https://pan.baidu.com/s/1axC_ob3UX8R_RlPxX3i_6Q?pwd=ano6 提取码：ano6 
> - MAC版链接：https://pan.baidu.com/s/1SCt2MkXYWV0qsdPTqpid2w?pwd=ano6 提取码：ano6 
> - 更新内容：
> - - 小工具命令按钮（自定义命令）功能，增加命令保存数量至50组。

##### 版本：V1.0.2
> - 更新时间：20230324
> - 链接：https://pan.baidu.com/s/1-kbHs2C4vzQh1ZXWYRUhzA?pwd=ano6 提取码：ano6 
> - 更新内容：
> - - 增加皮肤功能，修复0x32和0x33帧数据交叉的BUG。


