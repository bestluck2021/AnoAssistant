# 匿名助手

#### 介绍
匿名团队打造的调试软件，可以实现串口、UDP方式的数据解析、波形显示等丰富功能，其设计充分考虑了软件的通用性、灵活性，希望能作为一个好用的调试助手帮助到大家的开发、调试工作。
![主界面](.gitee/mainUI.png)
![数据解析](.gitee/mainUI_rx2.png)
![数据波形](.gitee/mainUI_plot.png)
![三维模型](.gitee/mainUI_3d.png)
![实用工具](.gitee/mainUI_tools.png)
有人说“软件越粉，参数越稳”，所以，加上了网友制作的粉色皮肤《猛男粉》
![猛男粉](.gitee/mainUI_pink.JPG)
#### 使用说明

1.  编辑中

#### 实现教程

##### 1. 了解各个文件的作用
- AnoPTv8.h
包含协议相关函数的声明、硬件ID、版本号等的声明，在外部需要调用匿名V8协议的地方引用AnoPTv8.h即可。
- hardwareInterface.c/h
外部需要实现的相关函数，包括串口收发、周期调用任务函数。
- AnoPTv8DataDef.c/h
匿名V8协议中的数据定义、参数定义、命令定义。
- AnoPTv8Run.c/h
匿名V8协议主要运行相关函数，包含数据解析判断等。
- AnoPTv8FrameFactory.c/h
各个数据帧的打包发送函数。

##### 2. 实现串口收发及周期调用函数
匿名协议的正常运行，仅需要实现3个外部接口函数：
- void AnoPTv8HwSendBytes(uint8_t *buf, uint16_t len)

数据发送函数，一般为设备的串口发送函数，注意，这里是数组多字节发送，所以设备的串口驱动必须为中断+环形缓冲或者DMA+缓冲的发送模式，单字节或阻塞式发送函数，将大量占用MCU时间。
- void AnoPTv8HwRecvByte(uint8_t dat)

此函数传入的是字节数据，如果接收事件接收到的数据大于1字节，多次调用此函数即可。
- void AnoPTv8HwTrigger1ms(void)

用户需要在1ms定时中断或者系统滴答或者自己设计的调度器内，以1ms的时间间隔调用此函数。

##### 3.  测试
此时打开匿名助手，打开对应的串口连接，点击读取设备信息或读取参数，即可完成基于匿名V8协议的通信功能。
使用示例程序里的AnoPTv8Test.c/h内AnoPTv8TxFrameF1、AnoPTv8TxFrameF2函数，可实现灵活数据的上传功能，注意上位机要根据F1和F2帧的内容正确配置数据。


#### 视频介绍：[视频介绍](https://www.bilibili.com/video/BV1sb411f7VF/?vd_source=85d0827f0b576f52b68a8b795e49826a#reply225753889)


